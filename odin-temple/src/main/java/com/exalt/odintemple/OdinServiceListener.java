package com.exalt.odintemple;

import com.exalt.odintemple.data.*;
import com.exalt.odintemple.feignclient.OdinTemple;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OdinServiceListener {
    private final OdinTemple odinTemple ;
    private final ObjectMapper mapper;
    @KafkaListener(topics = "odin_ravens") //odin_ravens
    public void listenGroupFoo( @Payload String message) throws JsonProcessingException {
        var response = mapper.readValue(message, DrakkarResponseNjord.class);
        System.out.println("Received Message in odin_ravens: " + message);
        if (response.getNjordResponse() == NjordResponse.NjordRejected) {
            odinTemple.sendOffering(new AskOdinPriestToWriteRequest(response.getRaidId(), response.getClanId(), "NjordRejected",
                    "NotAttemptedHerja", "NotAttemptedVillage", "RaidFailure"));
        }
    }
}