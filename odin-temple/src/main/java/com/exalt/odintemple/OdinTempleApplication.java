package com.exalt.odintemple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OdinTempleApplication {

	public static void main(String[] args) {
		SpringApplication.run(OdinTempleApplication.class, args);
	}

}
