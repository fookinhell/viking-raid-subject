package com.exalt.odintemple.data;

public class RavenFromTheVillage {

    private String raidId;

    private String clanId;

    private String villageOutcome;

    public RavenFromTheVillage(String raidId, String clanId, String villageOutcome) {
        this.raidId = raidId;
        this.clanId = clanId;
        this.villageOutcome = villageOutcome;
    }
}
