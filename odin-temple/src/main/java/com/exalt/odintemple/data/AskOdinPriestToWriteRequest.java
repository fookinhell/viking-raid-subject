package com.exalt.odintemple.data;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AskOdinPriestToWriteRequest {

    private String raidId;

    private String clanId;

    private String njordAnswer;

    private String herjaAnswer;

    private String villageOutcome;

    private String raidOutcome;
}
