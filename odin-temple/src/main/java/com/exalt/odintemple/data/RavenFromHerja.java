package com.exalt.odintemple.data;


public class RavenFromHerja {

    private String raidId;

    private String clanId;

    private String herjaAnswer;

    public RavenFromHerja(String raidId, String clanId, String herjaAnswer) {
        this.raidId = raidId;
        this.clanId = clanId;
        this.herjaAnswer = herjaAnswer;
    }
}
