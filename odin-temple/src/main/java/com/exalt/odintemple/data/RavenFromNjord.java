package com.exalt.odintemple.data;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RavenFromNjord {

    private String raidId;

    private String clanId;

    private String njordAnswer;

}
