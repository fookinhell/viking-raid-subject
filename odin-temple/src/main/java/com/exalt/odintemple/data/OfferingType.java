package com.exalt.odintemple.data;

public enum OfferingType {
    Goat,
    Cow,
    Fish,
    Grain,
    Wine,
    Beer,
    Gold,
    Nothing
}
