package com.exalt.odintemple.data;

public enum NjordResponse {
    NjordAccepted, NjordRejected
}
