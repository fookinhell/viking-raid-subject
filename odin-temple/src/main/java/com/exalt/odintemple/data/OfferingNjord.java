package com.exalt.odintemple.data;

import lombok.Data;

@Data
public class OfferingNjord {
    private final String raidId;
    private final String clanId;
    private final OfferingType offeringType;

}
