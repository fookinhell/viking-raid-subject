package com.exalt.odintemple.feignclient;

import com.exalt.odintemple.data.AskOdinPriestToWriteRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "writeBook", url = "http://localhost:81", configuration = ClientConfiguration.class)
public interface OdinTemple {

    @RequestMapping(method = RequestMethod.PATCH, value = "/write_in_the_book_of_destiny", produces = "application/json")
    String sendOffering(AskOdinPriestToWriteRequest offering);

}