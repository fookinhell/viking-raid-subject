package com.exalt.viking.navigationservice.feignclient;

import com.exalt.viking.navigationservice.dto.OfferingNjord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "njordtemple", url = "http://localhost", configuration = ClientConfiguration.class)
public interface NjordTemple {

    @RequestMapping(method = RequestMethod.POST, value = "/offerings_to_njord", produces = "application/json")
    String sendOffering(OfferingNjord offering);

}