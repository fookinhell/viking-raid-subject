package com.exalt.viking.navigationservice.dto;

public enum OfferingType {
    Goat,
    Cow,
    Fish,
    Grain,
    Wine,
    Beer,
    Gold,
    Nothing
}
