package com.exalt.viking.navigationservice.dto;

public enum NjordResponse {
    NjordAccepted, NjordRejected
}
