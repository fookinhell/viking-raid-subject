package com.exalt.viking.navigationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class NavigationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NavigationServiceApplication.class, args);
	}

}
