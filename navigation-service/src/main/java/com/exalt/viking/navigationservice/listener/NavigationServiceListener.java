package com.exalt.viking.navigationservice.listener;

import com.exalt.viking.navigationservice.dto.*;
import com.exalt.viking.navigationservice.feignclient.NjordTemple;
import com.exalt.viking.navigationservice.utils.RandomEnumGenerator;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NavigationServiceListener {

    private final NjordTemple njordTemple;
    private final KafkaTemplate<String, DrakkarResponseNjord> kafkaTemplate;

    public void sendMessage(DrakkarResponseNjord msg) {
        kafkaTemplate.send("odin_ravens", msg);
    }

    RandomEnumGenerator reg = new RandomEnumGenerator(OfferingType.class);

    @KafkaListener(topics = "thor_voice", containerFactory = "kafkaListenerContainerFactory")
    public void listenGroupFoo(Drakkar drakkar) {
        System.out.println("Received Drakkar in group foo: " + drakkar);
        OfferingNjord offeringNjord = new OfferingNjord(drakkar.getRaidId(), drakkar.getClanId(), randomOfferingType());
        String response = null;
        try {
            response = njordTemple.sendOffering(offeringNjord);
        } catch (FeignException.FeignClientException e) {
            sendMessage(new DrakkarResponseNjord(drakkar.getRaidId(), drakkar.getClanId(), NjordResponse.NjordRejected));
            return;
        }
        System.out.println("NTM" + response);
        //publish
        sendMessage(new DrakkarResponseNjord(drakkar.getRaidId(), drakkar.getClanId(), NjordResponse.NjordAccepted));
    }

    private OfferingType randomOfferingType() {
        return (OfferingType) reg.randomEnum();
    }
}
