package com.exalt.viking.navigationservice.dto;

import lombok.Data;

@Data
public class Drakkar {
    private String raidId;
    private String clanId;

}
